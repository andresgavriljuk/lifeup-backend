<?php

use App\Actor;
use App\Role;
use Illuminate\Database\Seeder;

class ActorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'first_name' => 'Aleksander',
                'last_name' => 'Skafander',
                'position' => 'Programmijuhi abi',
                'department' => 'Infotehnoloogia teaduskonna dekanaat',
                'employee_id' => 'd543193bdf256363bf956f83ebd288e53000fe48',
                'role' => Actor::ROLE_EMPLOYEE
            ],
            [
                'first_name' => 'Aleksandra',
                'last_name' => 'Saar',
                'position' => 'Infotehnoloogia teaduskonna dekanaati professor',
                'department' => 'Infotehnoloogia teaduskonna dekanaat',
                'employee_id' => '2eddfdbbbbcc974ff8c6edc3b7e877396fd86788',
                'role' => Actor::ROLE_MANAGER
            ],
            [
                'first_name' => 'Aleksandra',
                'last_name' => 'Kuuseok',
                'position' => 'Raamatupidaja',
                'department' => 'Rahandusosakond',
                'employee_id' => '91adb61eced7eec14a3936da41ad8e0c0cdd76e3',
                'role' => Actor::ROLE_ACCOUNTANT
            ],
        ];
        foreach ($data as $datum) {
            Actor::create($datum);
        }
    }
}
