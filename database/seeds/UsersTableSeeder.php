<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userData = [
            [
                'user_id' => 'd543193bdf256363bf956f83ebd288e53000fe48',
                'first_name' => 'Aleksander',
                'last_name' => 'Skafander',
                'position' => 'programmijuhi abi',
                'department' => 'Infotehnoloogia teaduskonna dekanaat',
                'name' => 'aleksander',
                'password' => Hash::make('skafander'),
                'api_token' => 123
            ],
            [
                'user_id' => '2eddfdbbbbcc974ff8c6edc3b7e877396fd86788',
                'first_name' => 'Aleksandra',
                'last_name' => 'Saar',
                'position' => 'Infotehnoloogia teaduskonna dekanaati professor',
                'department' => 'Infotehnoloogia teaduskonna dekanaat',
                'name' => 'saar',
                'password' => Hash::make('saar'),
                'api_token' => 456
            ],
            [
                'user_id' => '91adb61eced7eec14a3936da41ad8e0c0cdd76e3',
                'first_name' => 'Aleksandra',
                'last_name' => 'Kuuseok',
                'position' => 'Raamatupidaja',
                'department' => 'Rahandusosakond',
                'name' => 'kuuseok',
                'password' => Hash::make('kuuseok'),
                'api_token' => 789
            ],
            [
                'user_id' => '2af8a7f3e96a7b2f3aca2eb58772754d075ab3b6',
                'first_name' => 'Ben',
                'last_name' => 'Jonson',
                'position' => 'administratiivjuht',
                'department' => 'Infotehnoloogia teaduskonna dekanaat',
                'name' => '',
                'password' => '123',
            ],
            [
                'user_id' => '0c8aa61a73a10990ec0ffeb22ee1be4c340f560f',
                'first_name' => 'Donald',
                'last_name' => 'Mets',
                'position' => 'õppekonsultant',
                'department' => 'Infotehnoloogia teaduskonna dekanaat',
                'name' => '',
                'password' => '123',
            ],
            [
                'user_id' => '6ea7a132455e31733a12a941e126992f997eacb0',
                'first_name' => 'Teresa',
                'last_name' => 'Kiisu',
                'position' => 'programmijuhi abi',
                'department' => 'Infotehnoloogia teaduskonna dekanaat',
                'name' => '',
                'password' => '123',
            ],
            [
                'user_id' => 'e51c2f7cdd4d066b0bb88746f953408589546b5a',
                'first_name' => 'Aleksei',
                'last_name' => 'Tamm',
                'position' => 'õppekonsultant',
                'department' => 'Infotehnoloogia teaduskonna dekanaat',
                'name' => '',
                'password' => '123',
            ],
        ];
        foreach ($userData as $datum) {
            User::create($datum);
        }
    }
}
