<?php

use Illuminate\Database\Seeder;

class VacationDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\VacationDatum::create([
            'employee_id' => 'd543193bdf256363bf956f83ebd288e53000fe48',
            'used_vacation_days' => 0,
            'fte' => 1,
            'holiday_scheme' => 35
        ]);
    }
}
