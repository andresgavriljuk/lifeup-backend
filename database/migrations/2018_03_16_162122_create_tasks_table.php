<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('status')->nullable();
            $table->integer('deadline');
            $table->integer('process_id')->unsigned();
            $table->integer('creator_id')->unsigned();
            $table->integer('assignee_id')->unsigned();
            $table->timestamps();
            $table->foreign('process_id')->references('id')->on('processes');
            $table->foreign('creator_id')->references('id')->on('actors');
            $table->foreign('assignee_id')->references('id')->on('actors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
