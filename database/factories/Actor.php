<?php

use App\Actor;
use Faker\Generator as Faker;

$factory->define(Actor::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'position' => $faker->text,
        'department' => $faker->text,
        'employee_id' => str_random(60),
        'role' => $faker->randomElement([Actor::ROLE_EMPLOYEE, Actor::ROLE_MANAGER, Actor::ROLE_ACCOUNTANT])
    ];
});
