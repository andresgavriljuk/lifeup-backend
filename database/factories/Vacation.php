<?php

use Faker\Generator as Faker;

$factory->define(\App\Vacation::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement(\App\VacationType::toArray()),
        'start_date' => $faker->dateTimeBetween('+10 days', '+30 days')->format('d.m.Y'),
        'end_date' => $faker->dateTimeBetween('+30 days', '+60 days')->format('d.m.Y'),
        'comment' => $faker->text,
        'children_14' => $faker->numberBetween(0, 4),
        'children_3' => $faker->numberBetween(0, 4),
        'child_name' => $faker->name,
        'child_personal_code' => $faker->randomNumber(),
        'employee_id' => 1
    ];
});
