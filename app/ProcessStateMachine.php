<?php


namespace App;


use App\Exceptions\NotAllowedException;

final class ProcessStateMachine
{
    const STATE_CREATED = 'created';

    const STATE_APPROVED_BY_MANAGER = 'approved_by_manager';
    const STATE_REJECTED_BY_MANAGER = 'rejected_by_manager';
    const STATE_APPROVED_BY_ACCOUNTANT = 'approved_by_accountant';
    const STATE_REJECTED_BY_ACCOUNTANT = 'rejected_by_accountant';

    private $transitions = [
        self::STATE_CREATED => [ self::STATE_APPROVED_BY_MANAGER, self::STATE_REJECTED_BY_MANAGER],
        self::STATE_APPROVED_BY_MANAGER => [self::STATE_APPROVED_BY_ACCOUNTANT, self::STATE_REJECTED_BY_ACCOUNTANT],
        self::STATE_REJECTED_BY_MANAGER => [],
        self::STATE_APPROVED_BY_ACCOUNTANT => [],
        self::STATE_REJECTED_BY_ACCOUNTANT => []
    ];

    /** @var Process */
    private $process;

    /**
     * ProcessStateMachine constructor.
     * @param Process $process
     */
    public function __construct(Process $process)
    {
        $this->process = $process;
    }


    public function canSetState($state) {
        $currentState = $this->getCurrentState();
        return in_array($state, $this->transitions[$currentState]);
    }

    /**
     * @param Process $process
     * @param $state
     * @param Actor $actor
     * @throws NotAllowedException
     */
    public function setState(Process $process, $state, Actor $actor) {
        if(!$this->canSetState($state)) {
            throw new NotAllowedException("State is not allowed");
        }
        $task = $process->tasks()->get()->last();
        switch ($state) {
            case self::STATE_APPROVED_BY_MANAGER:
                $task->status = TaskStatus::APPROVED;
                $task->save();

                $nextAssignee = Actor::whereRole(Actor::ROLE_ACCOUNTANT)->get()->first();
                Task::create([
                    'type' => TaskType::TO_ACCOUNTANT,
                    'assignee_id' => $nextAssignee->id,
                    'process_id' => $process->id,
                    'creator_id' => $actor->id
                ]);
                break;
            case self::STATE_REJECTED_BY_MANAGER:
                $task->status = TaskStatus::REJECTED;
                $task->save();
                $process->status = ProcessStatus::FINISHED;
                $process->save();
                $process->vacation->status = VacationStatus::REJECTED;
                $process->vacation->save();
                break;
            case self::STATE_APPROVED_BY_ACCOUNTANT:
                $task->status = TaskStatus::APPROVED;
                $task->save();
                $process->status = ProcessStatus::FINISHED;
                $process->save();
                $process->vacation->status = VacationStatus::APPROVED;
                $process->vacation->save();
                break;
            case self::STATE_REJECTED_BY_ACCOUNTANT:
                $task->status = TaskStatus::REJECTED;
                $task->save();
                $process->status = ProcessStatus::FINISHED;
                $process->save();
                $process->vacation->status = VacationStatus::REJECTED;
                $process->vacation->save();
                break;
        }

    }

    public function getCurrentState() {
        $tasks = $this->process->tasks()->get();
        if(!count($tasks)) return self::STATE_CREATED;
        $lastTask = $tasks->last();

        return $this->getStateByActorAndTaskStatus($lastTask->assignee, $lastTask->status);
    }

    /**
     * @param Actor $actor
     * @param $status
     * @return null|string
     */
    public function getStateByActorAndTaskStatus(Actor $actor, $status) {
        $states = [
            Actor::ROLE_MANAGER => [
                TaskStatus::APPROVED => self::STATE_APPROVED_BY_MANAGER,
                TaskStatus::REJECTED => self::STATE_REJECTED_BY_MANAGER,
                TaskStatus::CREATED => self::STATE_CREATED
            ],
            Actor::ROLE_ACCOUNTANT => [
                TaskStatus::APPROVED => self::STATE_APPROVED_BY_ACCOUNTANT,
                TaskStatus::REJECTED => self::STATE_REJECTED_BY_ACCOUNTANT,
                TaskStatus::CREATED => self::STATE_APPROVED_BY_MANAGER
            ]
        ];

        if(!array_key_exists($actor->role, $states)
            || !array_key_exists($status, $states[$actor->role])) {
            return null;
        }

        return $states[$actor->role][$status];
    }
}