<?php


namespace App;


use MyCLabs\Enum\Enum;

/**
 * Class VacationStatus
 * @package App
 *
 * @SWG\Definition(
 *     definition="vacation_status",
 *     type="string",
 *     enum={"draft","in_approval","approved","rejected"}
 * )
 */
class VacationStatus extends Enum
{
    const DRAFT = 'draft';
    const IN_APPROVAL = 'in_approval';
    const APPROVED = 'approved';
    const REJECTED = 'rejected';
}