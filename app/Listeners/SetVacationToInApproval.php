<?php


namespace App\Listeners;


use App\Events\ProcessCreated;
use App\Vacation;
use App\VacationStatus;

class SetVacationToInApproval
{
    public function handle(ProcessCreated $event) {
        /** @var Vacation $vacation */
        $vacation = $event->process->vacation;
        $vacation->status = VacationStatus::IN_APPROVAL;
        $vacation->save();
    }
}