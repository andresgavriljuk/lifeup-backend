<?php

namespace App;


use MyCLabs\Enum\Enum;

/**
 * Class TaskType
 * @package App
 *
 * @SWG\Definition(
 *     definition="task_type",
 *     type="string",
 *     enum={"to_manager","to_accountant"}
 * )
 */
class TaskType extends Enum
{
    const TO_MANAGER = 'to_manager';
    const TO_ACCOUNTANT = 'to_accountant';
}