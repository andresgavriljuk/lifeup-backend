<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Process
 * @package App
 *
 * @SWG\Definition()
 */
class Process extends Model
{
    /** @var ProcessStatus */
    /** @SWG\Property(type="string", enum={"running","finished"}) */
    protected $status;

    protected $guarded = ['status'];

    protected $attributes = ['status' => ProcessStatus::RUNNING];

    protected $hidden = ['vacation_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vacation()
    {
        return $this->belongsTo(Vacation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks() {
        return $this->hasMany(Task::class);
    }
}
