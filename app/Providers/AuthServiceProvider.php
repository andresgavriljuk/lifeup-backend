<?php

namespace App\Providers;

use App\Actor;
use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /** @var string */
    protected $role;

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('list-users', function () {
            return in_array($this->getRole(), [Actor::ROLE_MANAGER, Actor::ROLE_ACCOUNTANT]);
        });
        Gate::define('get-user', function () {
            return in_array($this->getRole(), [Actor::ROLE_MANAGER, Actor::ROLE_ACCOUNTANT]);
        });
        Gate::define('list-employees', function () {
            return in_array($this->getRole(), [Actor::ROLE_MANAGER, Actor::ROLE_ACCOUNTANT]);
        });
        Gate::define('create-vacation', function () {
            return in_array($this->getRole(), [Actor::ROLE_EMPLOYEE]);
        });
        Gate::define('create-process', function () {
            return $this->getRole() == Actor::ROLE_EMPLOYEE;
        });
        Gate::define('create-task', function () {
            return in_array($this->getRole(), [Actor::ROLE_MANAGER, Actor::ROLE_ACCOUNTANT]);
        });
    }

    protected function getRole() {
        if(!$this->role) {
            $id = request()->header('X-Actor-Id');
            $this->role = Actor::find($id)->role;
        }
        return $this->role;
    }
}
