<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class VacationDatum
 * @package App
 *
 * @SWG\Definition()
 */
class VacationDatum extends Model
{
    /** @var string */
    /** @SWG\Property(type="string") */
    protected $employee_id;

    /** @var integer */
    /** @SWG\Property(type="integer") */
    protected $holiday_scheme;

    /** @var integer */
    /** @SWG\Property(type="integer") */
    protected $fte;

    /** @var integer */
    /** @SWG\Property(type="integer") */
    protected $used_vacation_days;

    protected $fillable = ['employee_id', 'holiday_scheme', 'fte', 'used_vacation_days'];
}
