<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * Class Actor
 * @package App
 *
 * @SWG\Definition()
 */
class Actor extends Model
{
    const ROLE_EMPLOYEE = 'employee';
    const ROLE_MANAGER = 'manager';
    const ROLE_ACCOUNTANT = 'accountant';

    /** @var string */
    /** @SWG\Property(type="string") */
    protected $first_name;

    /** @var string */
    /** @SWG\Property(type="string") */
    protected $last_name;

    /** @var string */
    /** @SWG\Property(type="string") */
    protected $employee_id;

    /** @var string */
    /** @SWG\Property(type="string") */
    protected $position;

    /** @var string */
    /** @SWG\Property(type="string") */
    protected $department;

    /** @var string */
    /** @SWG\Property(type="enum", enum={"employee","manager","accountant"}) */
    protected $role;

    protected $fillable = ['first_name', 'last_name', 'employee_id', 'position', 'department', 'role'];
}
