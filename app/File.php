<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class File
 * @package App
 *
 * @SWG\Definition()
 */
class File extends Model
{
    /** @var string */
    /** @SWG\Property(type="string") */
    protected $orig_name;

    /** @var string */
    /** @SWG\Property(type="string") */
    protected $storage_name;

    /** @var string */
    /** @SWG\Property(type="string") */
    protected $token;

    protected $guarded = [];

    protected $hidden = ['storage_name', 'id'];

    /**
     * @param array $tokens
     * @param $vacation_id
     */
    public static function bindByTokensToVacation(array $tokens, $vacation_id)
    {
        foreach ($tokens as $token) {
            /** @var File $file */
            if(!$file = File::whereToken($token)->get()->first()) {
                throw new \InvalidArgumentException("File with token not found");
            }
            $file->vacation_id = $vacation_id;
            $file->save();
        }
    }

    public function vacation()
    {
        return $this->belongsTo(Vacation::class);
    }
}
