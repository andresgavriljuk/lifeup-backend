<?php


namespace App\Events;


use App\Process;

class ProcessCreated extends Event
{
    /** @var Process */
    public $process;

    /**
     * ProcessCreated constructor.
     * @param Process $process
     */
    public function __construct(Process $process)
    {
        $this->process = $process;
    }
}