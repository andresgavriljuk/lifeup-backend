<?php

namespace App\Rules;

use App\Vacation;
use App\VacationStatus;
use Illuminate\Contracts\Validation\Rule;

class VacationUnique implements Rule
{
    protected $employee_id;

    /**
     * Create a new rule instance.
     *
     * @param $employee_id
     */
    public function __construct($employee_id)
    {
        $this->employee_id = $employee_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Vacation::whereStatus(VacationStatus::DRAFT)->count() === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'There can not be two vacation requests registered on the same time';
    }
}
