<?php

namespace App\Rules;

use App\VacationType;
use Illuminate\Contracts\Validation\Rule;

class VacationStartDate extends VacationDate implements Rule
{
    protected $data;

    protected $messages = [];

    /**
     * Create a new rule instance.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        parent::__construct();
        $this->data = $data;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $now = new \DateTime();
        $date = new \DateTime($value);

        if ($date->diff($now)->days <= 0) {
            $this->messages[] = 'Start date must be after today';
            return false;
        }

        if($this->data['type'] == VacationType::PARENT) {
            if ($date->diff($now)->days < 14) {
                $this->messages[] = 'Less than 14 calendar days from today';
            }
        } else {
            if ($this->countBusinessDays($now, $date) < 10) {
                $this->messages[] = 'Less than 10 business days from today';
            }
        }

        if($this->messages) return false;

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return implode('. ', $this->messages);
    }
}
