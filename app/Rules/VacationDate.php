<?php


namespace App\Rules;


use Business\Business;
use Business\Day;
use Business\Days;

abstract class VacationDate
{
    protected $days = [];

    /**
     * VacationDate constructor.
     */
    public function __construct()
    {
        $this->days = [
            new Day(Days::MONDAY, [['0:00', '23:59']]),
            new Day(Days::TUESDAY, [['0:00', '23:59']]),
            new Day(Days::WEDNESDAY, [['0:00', '23:59']]),
            new Day(Days::THURSDAY, [['0:00', '23:59']]),
            new Day(Days::FRIDAY, [['0:00', '23:59']])
        ];
    }

    protected function countBusinessDays($startDate, $endDate) {
        $business = new Business($this->days);
        $dates = $business->timeline($startDate, $endDate, new \DateInterval('P1D'));
        return count($dates);
    }
}