<?php

namespace App\Rules;

use App\Actor;
use App\VacationDatum;
use App\VacationType;
use Illuminate\Contracts\Validation\Rule;

class VacationEndDate extends VacationDate implements Rule
{
    protected $data;

    protected $messages = [];
    /**
     * Create a new rule instance.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        parent::__construct();
        $this->data = $data;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $startDate = new \DateTime($this->data['start_date']);
        $endDate = new \DateTime($value);

        $employee = Actor::find($this->data['employee_id']);
        $vacationDatum = VacationDatum::where('employee_id', '=', $employee->employee_id)->get()->first();
        if(!$vacationDatum) {
            $this->messages[] = 'Cannot find vacation data for given employee';
        } else {
            if($vacationDatum['holiday_scheme'] * $vacationDatum['fte'] - $vacationDatum['used_vacation_days'] < 0) {
                $this->messages[] = 'The holiday period exceeds the available';
            }
            if($this->data['type'] == VacationType::MAIN && $endDate->diff($startDate)->days < 14) {
                $this->messages[] = 'The holiday period of first main holiday cannot be less than 14 days.';
            }
        }

        if($this->data['type'] == VacationType::MAIN && in_array($endDate->format('N'), [5])) {
            $this->messages[] = 'Cannot end on Friday';
        }

        if($this->messages) return false;

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return implode('. ', $this->messages);
    }
}
