<?php

namespace App\Console\Commands;

use App\VacationDatum;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;

class ImportVacationData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:import_vacation_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports vacation data from supplier';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = \config('app.vacation_supplier.url');
        $token = \config('app.vacation_supplier.token');
        $options = [
            CURLOPT_TIMEOUT => 50,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => [
                'Authorization: ' . $token
            ]
        ];
        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);

        if (false === $response) {
            echo "\nCurl error: " . curl_error($ch) . curl_errno($ch) . "\n";
            return false;
        }

        VacationDatum::truncate();

        $data = json_decode($response, true);

        foreach ($data as $datum) {
            VacationDatum::create([
                'employee_id' => $datum['employeeID'],
                'used_vacation_days' => $datum['usedVacationDays'],
                'fte' => $datum['FTE'],
                'holiday_scheme' => $datum['HolidayScheme']
            ]);
        }

        curl_close($ch);
        return true;
    }
}
