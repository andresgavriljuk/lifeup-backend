<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * Class Vacation
 * @package App
 *
 * @SWG\Definition()
 */
class Vacation extends Model
{
    /** @var VacationType */
    /** @var VacationStatus */
    /** @SWG\Property(type="enum", enum={"father","main","parent","kid","disabled_kid_parent","unpaid_kid","unpaid",
     *     "health_day","reducing_working_time","student"})
     */
    protected $type;

    /** @var string */
    /** @SWG\Property(type="string") */
    protected $reason;

    /** @SWG\Property(type="enum", enum={"draft","in_approval","approved","rejected"}) */
    protected $status;

    /** @var \DateTime */
    /** @SWG\Property(type="date") */
    protected $start_date;

    /** @var \DateTime */
    /** @SWG\Property(type="date") */
    protected $end_date;

    /** @var string */
    /** @SWG\Property(type="string") */
    protected $comment;

    /** @var integer */
    /** @SWG\Property(type="integer", default="0") */
    protected $children_14;

    /** @var integer */
    /** @SWG\Property(type="integer", default="0") */
    protected $children_3;

    /** @var string */
    /** @SWG\Property(type="string") */
    protected $child_name;

    /** @var string */
    /** @SWG\Property(type="string") */
    protected $child_personal_code;

    /** @var string */
    /** @SWG\Property(type="string") */
    protected $mother_id;

    protected $guarded = ['status'];

    protected $hidden = ['employee_id'];

    protected $attributes = ['status' => VacationStatus::DRAFT, 'children_3' => 0, 'children_14' => 0];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(Actor::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function processes()
    {
        return $this->hasMany(Process::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(File::class);
    }

//    public function getStartDateAttribute()
//    {
//        $date = new \DateTime($this->attributes['start_date']);
//        return $date->format('d.m.Y');
//    }
//
//    public function getEndDateAttribute()
//    {
//        $date = new \DateTime($this->attributes['end_date']);
//        return $date->format('d.m.Y');
//    }
//
//    public function setStartDateAttribute($value) {
//        $date = \DateTime::createFromFormat('d.m.Y', $value);
//        $this->attributes['start_date'] = $date->format('Y-m-d');
//    }
//
//    public function setEndDateAttribute($value) {
//        $date = \DateTime::createFromFormat('d.m.Y', $value);
//        $this->attributes['end_date'] = $date->format('Y-m-d');
//    }
}
