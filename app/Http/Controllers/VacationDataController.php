<?php

namespace App\Http\Controllers;

use App\VacationDatum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VacationDataController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request) {
        $employeeId = $request->query('employeeId');

        /** @var \Illuminate\Validation\Validator $validator */
        $validator = Validator::make($request->query(), [
            'employeeId' => 'required'
        ]);

        $validator->validate();

        return VacationDatum::whereEmployeeId($employeeId)->get()->first();
    }
}
