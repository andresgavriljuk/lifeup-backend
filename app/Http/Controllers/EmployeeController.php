<?php

namespace App\Http\Controllers;

use App\Actor;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function index() {
        return Actor::whereRole(Actor::ROLE_EMPLOYEE)->get()->all();
    }
}
