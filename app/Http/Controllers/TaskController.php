<?php

namespace App\Http\Controllers;

use App\Actor;
use App\Exceptions\NotAllowedException;
use App\Process;
use App\ProcessStateMachine;
use App\ProcessStatus;
use App\Task;
use App\TaskStatus;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class TaskController extends Controller
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index()
    {
        /** @var Builder $qb */
        $qb = Task::with(['creator', 'assignee', 'process', 'process.vacation']);
        /** @var \Illuminate\Validation\Validator $validator */
        $validator = Validator::make(request()->query(), [
            'status' => [
                'nullable',
                Rule::in(TaskStatus::toArray())
            ],
            'creatorId' => 'exists:actors,id',
            'assigneeId' => 'exists:actors,id',
            'vacationId' => 'exists:vacations,id'
        ]);

        $query = $validator->validate();

        if (isset($query['status'])) $qb->whereStatus($query['status']);
        if (isset($query['creatorId'])) $qb->whereCreatorId($query['creatorId']);
        if (isset($query['assigneeId'])) $qb->whereAssigneeId($query['assigneeId']);
        if (isset($query['vacationId'])) $qb->leftJoin('processes', 'tasks.process_id', '=', 'processes.id')
            ->where('processes.vacation_id', '=', $query['vacationId'])
            ->selectRaw("tasks.*");

        return $qb->get()->all();
    }

    /**
     * @param Process $process
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws UnprocessableEntityHttpException
     */
    public function create(Process $process, Request $request)
    {
        if ($process->status !== ProcessStatus::RUNNING) {
            throw new UnprocessableEntityHttpException("Process is not running");
        }

        /** @var Actor $actor */
        $actor = $request->actor;

        $payload = $request->all();
        if (!array_key_exists('status', $payload) || !in_array($payload['status'], TaskStatus::toArray())) {
            throw new UnprocessableEntityHttpException("Wrong status");
        }
        $stateMachine = new ProcessStateMachine($process);
        $state = $stateMachine->getStateByActorAndTaskStatus($actor, $payload['status']);

        if (!$stateMachine->canSetState($state)) {
            throw new UnprocessableEntityHttpException("Not allowed");
        }

        try {
            $stateMachine->setState($process, $state, $actor);
        } catch (NotAllowedException $exception) {
            throw new UnprocessableEntityHttpException($exception->getMessage());
        }

        $process->load('tasks');

        return response()->json($process, 201);
    }
}
