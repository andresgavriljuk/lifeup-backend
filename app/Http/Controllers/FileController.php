<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FileController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function upload(Request $request) {
        $uploadedFile = $request->file('document');
        $storedFile = $uploadedFile->store('documents');

        /** @var \Illuminate\Validation\Validator $validator */
        $validator = Validator::make($request->all(),[
            'document' => 'max:5120'
        ]);
//
        $validator->validate();

        $file = File::create([
            'orig_name' => $uploadedFile->getClientOriginalName(),
            'storage_name' => $storedFile,
            'token' => str_random(60)
        ]);

        return $file;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function download(Request $request) {
        $token = $request->offsetGet('token');

        $file = File::whereToken($token)->first();

        if(!$file) {
            throw new NotFoundHttpException("File not found");
        }

        return Storage::download($file->storage_name);
    }
}
