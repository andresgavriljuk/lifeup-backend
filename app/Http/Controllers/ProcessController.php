<?php

namespace App\Http\Controllers;

use App\Process;

class ProcessController extends Controller
{
    /**
     * @param Process $process
     * @return Process
     */
    public function show(Process $process) {
        $process->load(['tasks','tasks.assignee']);
        return $process;
    }
}
