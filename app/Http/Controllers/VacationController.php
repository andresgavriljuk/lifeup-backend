<?php

namespace App\Http\Controllers;

use App\Actor;
use App\Events\ProcessCreated;
use App\File;
use App\Process;
use App\Rules\VacationEndDate;
use App\Rules\VacationStartDate;
use App\Rules\VacationUnique;
use App\Task;
use App\TaskType;
use App\Vacation;
use App\VacationStatus;
use App\VacationType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Class VacationController
 * @package App\Http\Controllers
 *
 */
class VacationController extends Controller
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index()
    {
        $qb = Vacation::with('employee');
        /** @var \Illuminate\Validation\Validator $validator */
        $validator = Validator::make(request()->query(), [
            'status' => Rule::in(VacationStatus::toArray()),
            'creatorId' => 'exists:actors,id'
        ]);

        $query = $validator->validate();

        if (isset($query['status'])) $qb->whereStatus($query['status']);
        if (isset($query['creatorId'])) $qb->whereEmployeeId($query['creatorId']);

        return $qb->get()->all();
    }

    /**
     * @param Vacation $vacation
     * @return Vacation
     */
    public function show(Vacation $vacation)
    {
        $vacation->load(['employee', 'files', 'processes', 'processes.tasks']);
        return $vacation;
    }

    /**
     * @param Vacation $vacation
     * @return mixed
     */
    public function listProcesses(Vacation $vacation)
    {
        return $vacation->processes;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request)
    {
        $fileTokens = [];
        $request->merge([
            'employee_id' => $request->actor->id
        ]);
        $validator = $this->makeVacationValidator($request, true);
        $validatedData = $validator->validate();

        if (isset($validatedData['file_tokens'])) {
            $fileTokens = $validatedData['file_tokens'];
            unset($validatedData['file_tokens']);
        }

        /** @var Vacation $vacation */
        $vacation = Vacation::create($validatedData);
        File::bindByTokensToVacation($fileTokens, $vacation->id);

        return response()->json($vacation, 201);
    }

    /**
     * @param Request $request
     * @param bool $createMode
     * @return \Illuminate\Validation\Validator
     */
    protected function makeVacationValidator(Request $request, $createMode = false)
    {
        $config = [
            'type' => [
                'required',
                Rule::in(VacationType::toArray())
            ],
            'reason' => 'nullable',
            'start_date' => [
                'required',
//                'date_format:Y-m-d',
                new VacationStartDate($request->all())
            ],
            'end_date' => [
                'required',
//                'date_format:Y-m-d',
                'after:start_date',
                new VacationEndDate($request->all())
            ],
            'comment' => 'nullable',
            'children_3' => 'nullable|integer',
            'children_14' => 'nullable|integer',
            'child_name' => 'nullable',
            'child_personal_code' => 'nullable',
            'mother_id' => 'nullable',
            'employee_id' => [
                'required',
                'exists:actors,id',
            ],
            'file_tokens' => 'nullable|array'
        ];
//        if ($createMode) $config['employee_id'][] = new VacationUnique($request->actor->id);

        return Validator::make($request->all(), $config);
    }

    /**
     * @param Request $request
     * @param $id
     * @return Vacation
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $fileTokens = [];
        $request->merge([
            'employee_id' => $request->actor->id
        ]);
        /** @var Vacation $vacation */
        $vacation = Vacation::findOrFail($id);
        $validator = $this->makeVacationValidator($request);
        $validatedData = $validator->validate();

        if (isset($validatedData['file_tokens'])) {
            $fileTokens = $validatedData['file_tokens'];
            unset($validatedData['file_tokens']);
        }
        $vacation->update($validatedData);
        File::bindByTokensToVacation($fileTokens, $vacation->id);

        return $vacation;
    }

    /**
     * @param Vacation $vacation
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createProcess(Vacation $vacation, Request $request)
    {
        if ($vacation->status !== VacationStatus::DRAFT) {
            throw new UnprocessableEntityHttpException("Process can be started only for vacation in DRAFT status");
        }

        $process = Process::create([
            'vacation_id' => $vacation->id
        ]);

        $assignee = Actor::whereRole(Actor::ROLE_MANAGER)->get()->first();

        Task::create([
            'type' => TaskType::TO_MANAGER,
            'creator_id' => $request->actor->id,
            'assignee_id' => $assignee->id,
            'process_id' => $process->id,
            'deadline' => 3
        ]);

        $process->load('tasks');

        event(new ProcessCreated($process));

        return response()->json($process, 201);
    }
}
