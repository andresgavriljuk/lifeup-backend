<?php

namespace App\Http\Middleware;

use App\Actor;
use App\User;
use Closure;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class SetupActor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $actor_id = $request->header('X-Actor-Id');

        if(!$actor_id) {
            throw new UnauthorizedHttpException('X-Actor-Id','You must provide `X-Actor-Id` header with actor id');
        }

        $actor = Actor::find($actor_id);

        if (!$actor) {
            throw new UnauthorizedHttpException('X-Actor-Id','Actor with id:'.$actor_id.' not found');
        }

        $request->actor = $actor;

        return $next($request);
    }
}
