<?php


namespace App;


use MyCLabs\Enum\Enum;

/**
 * Class TaskStatus
 * @package App
 *
 * @SWG\Definition(
 *     definition="task_status",
 *     type="string",
 *     enum={"created","rejected","approved"}
 * )
 */
class TaskStatus extends Enum
{
    const CREATED = 'created';
    const REJECTED = 'rejected';
    const APPROVED = 'approved';
}