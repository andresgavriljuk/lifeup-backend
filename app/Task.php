<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Task
 * @package App
 *
 * @SWG\Definition()
 */
class Task extends Model
{
    /** @var string */
    /** @SWG\Property(type="string", enum={"to_accountant","to_manager"}) */
    protected $type;

    /** @var TaskStatus */
    /** @SWG\Property(type="string", enum={"created","approved","rejected"}) */
    protected $status;

    /** @var integer */
    /** @SWG\Property(type="integer") */
    protected $deadline;

    protected $attributes = ['status' => TaskStatus::CREATED, 'deadline' => 3];

    protected $guarded = ['status'];

    protected $hidden = ['assignee_id','creator_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assignee() {
        return $this->belongsTo(Actor::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function process() {
        return $this->belongsTo(Process::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator() {
        return $this->belongsTo(Actor::class);
    }
}
