<?php

use Swagger\Annotations as SWG;

/**
 * @SWG\SecurityScheme(
 *   securityDefinition="Bearer",
 *   type="apiKey",
 *   in="header",
 *   name="Authorization"
 * )
 * @SWG\SecurityScheme(
 *   securityDefinition="Actor",
 *   type="apiKey",
 *   in="header",
 *   name="X-Actor-Id"
 * )
 * @SWG\Swagger(
 *     basePath="/api",
 *     schemes={"http"},
 *     @SWG\Info(
 *          version="0.1.0",
 *          title="TTU-WEB API"
 *     ),
 *     security={{"Bearer": {}, "Actor": {}}},
 * )
 */

/** @SWG\Post(

 *     security={},
 *     path="/login",
 *     @SWG\Parameter(name="name",in="formData", type="string"),
 *     @SWG\Parameter(name="password",in="formData", type="string"),
 *     @SWG\Response(response=200, description="Successful operation"),
 *     @SWG\Response(response=422, description="The given data was invalid")
 * )
 */

/** @SWG\Get(
 *     description="Get list of actors",
 *     security={{"Bearer":{}}},
 *     path="/actors",
 *     @SWG\Response(response=200, description="Successful operation")
 * )
 */

/** @SWG\Get(
 *     description="Get list of employees",
 *     path="/employees",
 *     @SWG\Response(response=200, description="Successful operation"),
 *     @SWG\Response(response=401, description="Unauthorized or X-Actor-Id is not provided"),
 *     @SWG\Response(response=403, description="Forbidden for current actor")
 * )
 */

/** @SWG\Get(
 *     description="Get list of users",
 *     path="/users",
 *     @SWG\Response(response=200, description="Successful operation"),
 *     @SWG\Response(response=401, description="Unauthorized or X-Actor-Id is not provided"),
 *     @SWG\Response(response=403, description="Forbidden for current actor")
 * )
 */

/** @SWG\Get(
 *     path="/vacations",
 *     description="Get list of vacation requests",
 *     @SWG\Parameter(in="query",name="creatorId",type="integer"),
 *     @SWG\Parameter(in="query",name="status",type="string", enum={"draft","in_approval","approved","rejected"}),
 *     @SWG\Response(response=200, description="Successful operation"),
 *     @SWG\Response(response=401, description="Unauthorized or X-Actor-Id is not provided"),
 *     @SWG\Response(response=404, description="Wrong status or creator is not found")
 * )
 */

/** @SWG\Get(
 *     path="/vacations/{vacationId}",
 *     description="Show single vacation request",
 *     @SWG\Parameter(in="path",name="vacationId",type="integer"),
 *     @SWG\Response(response=200, description="Successful operation", @SWG\Schema(ref="#/definitions/Vacation")),
 *     @SWG\Response(response=401, description="Unauthorized or X-Actor-Id is not provided"),
 *     @SWG\Response(response=404, description="Not found")
 * )
 */

/**
 * @SWG\Post(
 *     path="/vacations",
 *     description="Create new vacation request",
 *
 *     @SWG\Parameter(in="body", name="vacation", @SWG\Schema(
 *           @SWG\Property(property="type", type="string", example="main", enum={"father","main","parent","kid",
 *     "disabled_kid_parent","unpaid_kid","unpaid","health_day","reducing_working_time","student"}),
 *           @SWG\Property(property="reason", type="string", example=""),
 *           @SWG\Property(property="status", type="string", enum={"draft","in_approval","approved","rejected"}, example="draft"),
 *           @SWG\Property(property="start_date", type="string", example="2018-04-20"),
 *           @SWG\Property(property="end_date", type="string", example="2018-10-20"),
 *           @SWG\Property(property="comment", type="string", example=""),
 *           @SWG\Property(property="children_14", type="integer", example=0),
 *           @SWG\Property(property="children_3", type="integer", example=0),
 *           @SWG\Property(property="child_name", type="string", example=""),
 *           @SWG\Property(property="child_personal_code", type="string", example=""),
 *           @SWG\Property(property="mother_id", type="string", example=""),
 *           @SWG\Property(property="file_tokens", type="array", example={}, items={"type":"string"}),
 *     )),
 *     @SWG\Response(response=201, description="Created"),
 *     @SWG\Response(response=401, description="Unauthorized or X-Actor-Id is not provided"),
 *     @SWG\Response(response=403, description="Forbidden for current actor"),
 *     @SWG\Response(response=422, description="Validation failed")
 * )
 */

/**
 * @SWG\Put(
 *     path="/vacations/{vacationId}",
 *     description="Update existing vacation request",
 *     @SWG\Parameter(in="path",name="vacationId",type="integer", required=true),
 *     @SWG\Parameter(in="body", name="vacation", @SWG\Schema(
 *           @SWG\Property(property="type", type="string", example="main", enum={"father","main","parent","kid",
 *     "disabled_kid_parent","unpaid_kid","unpaid","health_day","reducing_working_time","student"}),
 *           @SWG\Property(property="reason", type="string", example=""),
 *           @SWG\Property(property="status", type="string", enum={"draft","in_approval","approved","rejected"}, example="draft"),
 *           @SWG\Property(property="start_date", type="string", example="2018-04-20"),
 *           @SWG\Property(property="end_date", type="string", example="2018-10-20"),
 *           @SWG\Property(property="comment", type="string", example=""),
 *           @SWG\Property(property="children_14", type="integer", example=0),
 *           @SWG\Property(property="children_3", type="integer", example=0),
 *           @SWG\Property(property="child_name", type="string", example=""),
 *           @SWG\Property(property="child_personal_code", type="string", example=""),
 *           @SWG\Property(property="mother_id", type="string", example=""),
 *           @SWG\Property(property="file_tokens", type="array", example={}, items={"type":"string"}),
 *     )),
 *     @SWG\Response(response=201, description="Created"),
 *     @SWG\Response(response=401, description="Unauthorized or X-Actor-Id is not provided"),
 *     @SWG\Response(response=403, description="Forbidden for current actor"),
 *     @SWG\Response(response=404, description="Not found"),
 *     @SWG\Response(response=422, description="Validation failed")
 * )
 */

/**
 * @SWG\Post(
 *     path="/vacations/{vacationId}/processes",
 *     description="Start process on vacation request",
 *     @SWG\Parameter(in="path",name="vacationId",type="integer", required=true),
 *     @SWG\Response(response=201, description="Created"),
 *     @SWG\Response(response=401, description="Unauthorized or X-Actor-Id is not provided"),
 *     @SWG\Response(response=403, description="Forbidden for current actor"),
 *     @SWG\Response(response=422, description="Process can not be started")
 * )
 */

/** @SWG\Get(
 *     path="/processes/{processId}",
 *     description="Show single process",
 *     @SWG\Parameter(in="path",name="processId",type="integer"),
 *     @SWG\Response(response=200, description="Successful operation"),
 *     @SWG\Response(response=401, description="Unauthorized or X-Actor-Id is not provided"),
 *     @SWG\Response(response=404, description="Not found")
 * )
 */

/**
 * @SWG\Post(
 *     path="/processes/{processId}/tasks",
 *     description="Create new task",
 *     @SWG\Parameter(in="path", name="processId", type="integer", required=true),
 *     @SWG\Parameter(in="formData", name="status", type="string", required=true, enum={"approved", "rejected"}),
 *     @SWG\Response(response=201, description="Created"),
 *     @SWG\Response(response=401, description="Unauthorized or X-Actor-Id is not provided"),
 *     @SWG\Response(response=403, description="Forbidden for current actor"),
 *     @SWG\Response(response=422, description="Task can not be created")
 * )
 */

/** @SWG\Get(
 *     path="/tasks",
 *     description="Get list of tasks",
 *     @SWG\Parameter(in="query",name="status",type="string", enum={"created","approved","rejected"}),
 *     @SWG\Parameter(in="query",name="creatorId",type="integer"),
 *     @SWG\Parameter(in="query",name="assigneeId",type="integer"),
 *     @SWG\Parameter(in="query",name="vacationId",type="integer"),
 *     @SWG\Response(response=200, description="Successful operation"),
 *     @SWG\Response(response=401, description="Unauthorized or X-Actor-Id is not provided"),
 *     @SWG\Response(response=404, description="Wrong status or actor/vacation is not found")
 * )
 */

/**
 * @SWG\Post(
 *     path="/files",
 *     description="Upload new file",
 *     @SWG\Parameter(in="formData", name="document", type="file", required=true),
 *     @SWG\Response(response=201, description="Uploaded"),
 *     @SWG\Response(response=401, description="Unauthorized or X-Actor-Id is not provided"),
 *     @SWG\Response(response=422, description="Validation failed")
 * )
 */

/**
 * @SWG\Get(
 *     path="/files/{token}",
 *     description="Download file by token",
 *     @SWG\Parameter(in="path", name="token", type="string", required=true),
 *     @SWG\Response(response=200, description="File"),
 *     @SWG\Response(response=401, description="Unauthorized or X-Actor-Id is not provided"),
 *     @SWG\Response(response=404, description="Not found")
 * )
 */

/**
 * @SWG\Get(
 *     path="/vacation_data",
 *     description="Get vacation data",
 *     @SWG\Parameter(in="query", name="employeeId", type="string", required=true),
 *     @SWG\Response(response=200, description="Successful operation"),
 *     @SWG\Response(response=401, description="Unauthorized or X-Actor-Id is not provided"),
 *     @SWG\Response(response=403, description="Forbidden for current actor"),
 *     @SWG\Response(response=422, description="Validation failed ({employeeId} is not provided)")
 * )
 */

class Annotations
{
}
