<?php


namespace App;


use MyCLabs\Enum\Enum;
use Swagger\Annotations as SWG;

/**
 * Class VacationType
 * @package App
 *
 * @SWG\Definition(
 *     definition="vacation_type",
 *     type="string",
 *     enum={"father","main","parent","kid","disabled_kid_parent","unpaid_kid","unpaid",
 *          "health_day","reducing_working_time","student"}
 * )
 */
class VacationType extends Enum
{
    const FATHER = 'father';
    const MAIN = 'main';
    const PARENT = 'parent';
    const KID = 'kid';
    const DISABLED_KID_PARENT = 'disabled_kid_parent';
    const UNPAID_KID = 'unpaid_kid';
    const UNPAID = 'unpaid';
    const HEALTH_DAY = 'health_day';
    const REDUCING_WORKING_TIME = 'reducing_working_time';
    const STUDENT = 'student';

}