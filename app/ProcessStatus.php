<?php


namespace App;


use MyCLabs\Enum\Enum;

/**
 * Class ProcessStatus
 * @package App
 *
 * @SWG\Definition(
 *     definition="process_status",
 *     type="string",
 *     enum={"running","finished"}
 * )
 */
class ProcessStatus extends Enum
{
    const RUNNING = 'running';
    const FINISHED = 'finished';
}