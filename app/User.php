<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @package App
 *
 * @SWG\Definition()
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /** @var string */
    /** @SWG\Property(type="string") */
    protected $first_name;

    /** @var string */
    /** @SWG\Property(type="string") */
    protected $last_name;

    /** @var string */
    /** @SWG\Property(type="string") */
    protected $position;

    /** @var string */
    /** @SWG\Property(type="string") */
    protected $department;

    /** @var string */
    /** @SWG\Property(type="string") */
    protected $user_id;

    /** @SWG\Property(property="api_token", type="string") */

    /**
     * @return string
     */
    public function generateToken()
    {
        $this->api_token = str_random(60);
        $this->save();
        return $this->api_token;
    }
}
