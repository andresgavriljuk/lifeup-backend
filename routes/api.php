<?php

use Illuminate\Support\Facades\Route;

Route::post('login', 'Auth\LoginController@login');
Route::get('actors', 'ActorController@index')->middleware('auth:api');
Route::middleware(['auth:api', 'actor'])->group(function () {
    Route::get('users', 'UserController@index');
    Route::get('users/{user}', 'UserController@get');

    Route::get('employees', 'EmployeeController@index')
        ->middleware('can:list-employees');

    Route::get('vacations', 'VacationController@index');
    Route::post('vacations', 'VacationController@create')
        ->middleware('can:create-vacation');
    Route::put('vacations/{vacation}', 'VacationController@update')
        ->middleware('can:create-vacation');
    Route::get('vacations/{vacation}', 'VacationController@show');
    Route::get('vacations/{vacation}/processes', 'VacationController@listProcesses');
    Route::post('vacations/{vacation}/processes', 'VacationController@createProcess')
        ->middleware('can:create-process');
    Route::get('processes/{process}', 'ProcessController@show');

    Route::post('processes/{process}/tasks', 'TaskController@create')
        ->middleware('can:create-task');

    Route::get('tasks', 'TaskController@index');

    Route::post('files', 'FileController@upload');
    Route::get('files/{token}', 'FileController@download');

    Route::get('vacation_data', 'VacationDataController@index');
});
