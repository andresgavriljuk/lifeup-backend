<?php

namespace Tests\Feature;

use App\Actor;
use App\ProcessStatus;
use App\Vacation;
use App\VacationStatus;
use App\VacationType;
use Tests\AuthorizedTestCase;

class VacationTest extends AuthorizedTestCase
{
    public function testVacationsAreListedCorrectly()
    {
        $headers = $this->authorizeAndSetupActor(Actor::ROLE_EMPLOYEE);

        $vacations = Vacation::all();

        $this->json('GET', '/api/vacations', [], $headers)
            ->assertStatus(200)
            ->assertJson($vacations->toArray());
    }

    public function testVacationIsShowedCorrectly()
    {
        $headers = $this->authorizeAndSetupActor(Actor::ROLE_EMPLOYEE);
        $actor_id = $headers['X-Actor-Id'];
        $vacation = factory(Vacation::class)->create(['employee_id' => $actor_id]);
        $vacation->load('employee');

        $this->json('GET', '/api/vacations/' . $vacation->id, [], $headers)
            ->assertStatus(200)
            ->assertJson($vacation->toArray());
    }

    public function testVacationIsCreatedCorrectly()
    {
        $headers = $this->authorizeAndSetupActor(Actor::ROLE_EMPLOYEE);
        $payload = [
            'name' => 'name',
            'type' => VacationType::MAIN,
            'start_date' => '2018-04-06',
            'end_date' => '2018-04-21',
            'comment' => 'test',
            'children_14' => null,
            'children_3' => 1,
            'child_name' => null,
            'child_personal_code' => '123'
        ];
        $this->json('POST', '/api/vacations', $payload, $headers)
            ->assertStatus(201)
            ->assertJson(array_merge($payload, ['status' => VacationStatus::DRAFT]));
    }

    public function testVacationStartDateEarlier10BusinessDaysIsNotAllowed()
    {

    }

    public function testVacationStartDateEarlier14DaysForParentIsNotAllowed()
    {

    }

    public function testMainVacationCannotEndOnFridayOrMondayOrThursday()
    {

    }

    public function testMainVacationPeriodNotLessThan14Days()
    {

    }

    public function testVacationPeriodLimitExceeded()
    {

    }

    public function testTwoSameVacationsIsNotAllowed()
    {

    }

    public function testProcessesAreListedCorrectly()
    {
        $headers = $this->authorizeAndSetupActor(Actor::ROLE_MANAGER);
        $employee = factory(Actor::class)->create(['role' => Actor::ROLE_EMPLOYEE]);
        $vacation = factory(Vacation::class)->create(['employee_id' => $employee]);

        $this->json('GET', '/api/vacations/' . $vacation->id . '/processes', [], $headers)
            ->assertStatus(200);
    }

    public function testProcessAreStartedCorrectly()
    {
        $headers = $this->authorizeAndSetupActor(Actor::ROLE_EMPLOYEE);
        $vacation = factory(Vacation::class)->create(['employee_id' => $headers['X-Actor-Id']]);

        $this->json('POST', '/api/vacations/' . $vacation->id . '/processes', [], $headers)
            ->assertStatus(201)
            ->assertJsonFragment(['status' => ProcessStatus::RUNNING]);

        $vacation->refresh();
        $this->assertEquals(VacationStatus::IN_APPROVAL, $vacation->status);

        $this->json('POST', '/api/vacations/' . $vacation->id . '/processes', [], $headers)
            ->assertStatus(422);
    }
}
