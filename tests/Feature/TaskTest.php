<?php

namespace Tests\Feature;

use App\Actor;
use App\Process;
use App\ProcessStatus;
use App\Task;
use App\TaskStatus;
use App\Vacation;
use App\VacationStatus;
use Tests\AuthorizedTestCase;

class TaskTest extends AuthorizedTestCase
{
    public function testTasksAreListedCorrectly()
    {
        $headers = $this->authorizeAndSetupActor(Actor::ROLE_EMPLOYEE);
        $tasks = Task::all();
        $this->json('GET', '/api/tasks', [], $headers)
            ->assertStatus(200)
            ->assertJson($tasks->toArray());
    }

    public function testManagerTaskApprovedCorrectly()
    {
        $headers = $this->authorizeAndSetupActor(Actor::ROLE_MANAGER);
        $employee = Actor::whereRole(Actor::ROLE_EMPLOYEE)->get()->first();
        $vacation = factory(Vacation::class)->create(['status' => VacationStatus::IN_APPROVAL]);
        $process = Process::create([
            'status' => ProcessStatus::RUNNING,
            'vacation_id' => $vacation->id
        ]);
        Task::create([
            'creator_id' => $employee->id,
            'assignee_id' => $headers['X-Actor-Id'],
            'process_id' => $process->id
        ]);
        $payload = ['status' => TaskStatus::APPROVED];
        $this->json('POST', '/api/processes/' . $process->id . '/tasks', $payload, $headers)
            ->assertStatus(201);
        $vacation->refresh();
        $process->refresh();
        $this->assertEquals(VacationStatus::IN_APPROVAL, $vacation->status);
        $this->assertEquals(ProcessStatus::RUNNING, $process->status);
    }

    public function testManagerTaskRejectedCorrectly()
    {
        $headers = $this->authorizeAndSetupActor(Actor::ROLE_MANAGER);
        $employee = Actor::whereRole(Actor::ROLE_EMPLOYEE)->get()->first();
        $vacation = factory(Vacation::class)->create(['status' => VacationStatus::IN_APPROVAL]);
        $process = Process::create([
            'status' => ProcessStatus::RUNNING,
            'vacation_id' => $vacation->id
        ]);
        Task::create([
            'creator_id' => $employee->id,
            'assignee_id' => $headers['X-Actor-Id'],
            'process_id' => $process->id
        ]);
        $payload = ['status' => TaskStatus::REJECTED];
        $this->json('POST', '/api/processes/' . $process->id . '/tasks', $payload, $headers)
            ->assertStatus(201);
        $vacation->refresh();
        $process->refresh();
        $this->assertEquals(VacationStatus::REJECTED, $vacation->status);
        $this->assertEquals(ProcessStatus::FINISHED, $process->status);
    }

    public function testAccountantTaskApprovedCorrectly()
    {
        $headers = $this->authorizeAndSetupActor(Actor::ROLE_ACCOUNTANT);
        $employee = Actor::whereRole(Actor::ROLE_EMPLOYEE)->get()->first();
        $manager = Actor::whereRole(Actor::ROLE_MANAGER)->get()->first();
        $vacation = factory(Vacation::class)->create(['status' => VacationStatus::IN_APPROVAL]);
        $process = Process::create([
            'status' => ProcessStatus::RUNNING,
            'vacation_id' => $vacation->id
        ]);
        $task = Task::create([
            'creator_id' => $employee->id,
            'assignee_id' => $manager->id,
            'process_id' => $process->id,
            'status' => TaskStatus::APPROVED
        ]);
        $task->status = TaskStatus::APPROVED;
        $task->save();

        Task::create([
            'creator_id' => $manager->id,
            'assignee_id' => $headers['X-Actor-Id'],
            'process_id' => $process->id,
        ]);
        $payload = ['status' => TaskStatus::APPROVED];
        $this->json('POST', '/api/processes/' . $process->id . '/tasks', $payload, $headers)
            ->assertStatus(201);
        $vacation->refresh();
        $process->refresh();
        $this->assertEquals(VacationStatus::APPROVED, $vacation->status);
        $this->assertEquals(ProcessStatus::FINISHED, $process->status);
    }

    public function testAccountantTaskRejectedCorrectly()
    {
        $headers = $this->authorizeAndSetupActor(Actor::ROLE_ACCOUNTANT);
        $employee = Actor::whereRole(Actor::ROLE_EMPLOYEE)->get()->first();
        $manager = Actor::whereRole(Actor::ROLE_MANAGER)->get()->first();
        $vacation = factory(Vacation::class)->create(['status' => VacationStatus::IN_APPROVAL]);
        $process = Process::create([
            'status' => ProcessStatus::RUNNING,
            'vacation_id' => $vacation->id
        ]);
        $task = Task::create([
            'creator_id' => $employee->id,
            'assignee_id' => $manager->id,
            'process_id' => $process->id
        ]);
        $task->status = TaskStatus::APPROVED;
        $task->save();
        Task::create([
            'creator_id' => $manager->id,
            'assignee_id' => $headers['X-Actor-Id'],
            'process_id' => $process->id,
        ]);
        $payload = ['status' => TaskStatus::REJECTED];
        $this->json('POST', '/api/processes/' . $process->id . '/tasks', $payload, $headers)
            ->assertStatus(201);
        $vacation->refresh();
        $process->refresh();
        $this->assertEquals(VacationStatus::REJECTED, $vacation->status);
        $this->assertEquals(ProcessStatus::FINISHED, $process->status);
    }
}
