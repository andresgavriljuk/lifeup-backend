<?php

namespace Tests\Feature;

use App\Actor;
use App\Process;
use App\ProcessStatus;
use App\Vacation;
use App\VacationStatus;
use Tests\AuthorizedTestCase;

class ProcessTest extends AuthorizedTestCase
{
    public function testProcessIsShowedCorrectly()
    {
        $headers = $this->authorizeAndSetupActor(Actor::ROLE_EMPLOYEE);
        $vacation = factory(Vacation::class)->create();
        $process = Process::create([
            'status' => ProcessStatus::RUNNING,
            'vacation_id' => $vacation->id
        ]);

        $r = $this->json('GET', '/api/processes/'.$process->id, [], $headers)
            ->assertStatus(200)
            ->assertJson($process->toArray());
    }

    public function testProcessIsStartedCorrectly()
    {
        $headers = $this->authorizeAndSetupActor(Actor::ROLE_EMPLOYEE);
        $vacation = factory(Vacation::class)->create();
        $r = $this->json('POST', '/api/vacations/' . $vacation->id . '/processes', [], $headers)
            ->assertStatus(201)
            ->assertJsonStructure(['status', 'updated_at', 'created_at', 'tasks']);

        $vacation->refresh();

        $this->assertEquals($vacation['status'], VacationStatus::IN_APPROVAL);
    }
}
