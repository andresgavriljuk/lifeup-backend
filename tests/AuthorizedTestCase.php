<?php


namespace Tests;


use App\Actor;
use App\User;

abstract class AuthorizedTestCase extends TestCase
{
    /**
     * @param $actorRole
     * @return array Headers
     */
    protected function authorizeAndSetupActor($actorRole) {
        $user = factory(User::class)->create();
        $actor = Actor::create([
            'first_name' => 'test',
            'last_name' => 'test',
            'employee_id' => 'd543193bdf256363bf956f83ebd288e53000fe48',
            'position' => 'test',
            'department' => 'test',
            'role' => $actorRole
        ]);
        $token = $user->generateToken();

        return [
            'Authorization' => "Bearer $token",
            'X-Actor-Id' => $actor->id
        ];
    }
}