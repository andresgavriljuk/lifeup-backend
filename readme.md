
## Installation ###

Create new file `.env` from `.env.example` and change database settings:
````
cp .env.example .env
````

Install vendor dependencies:
````
composer install
````

Create tables and test data:
````
php artisan migration:fresh
php artisan db:seed
````

Download vacation data and put into database for local usage:
````
php artisan command:import_vacation_data
````

Run tests:
````
composer test
````

## Running

### Built-in web-server

Just run:
````
php artisan serve
````
````
Laravel development server started: <http://127.0.0.1:8000>
````

### Nginx

Standard nginx configuration for PHP application should be used.
Root web server directory: `./public/`

There is example nginx configuration file: `nginx.conf`

## Front-end assets

Any javascript, CSS and HTML files can be copied to `./public` directory.

Also you can put your frontend application into any separate directory and tell nginx
to use it, for example:
````
location /app/ {
    alias /home/developer/frontend-public-dir
    
    # redirect all request to index.html
    try_files $uri $uri/ /app/index.html
}
````

## Using Swagger ###

Generate Swagger documentation:
````
php artisan l5-swagger:generate
````

After installation Swagger UI will be available here:
````
http://host/api/documentation
```` 

### Authentication and authorization ###

Credentials for testing:
```json
[{
  "name": "aleksander",
  "password": "skafander",
  "api_token": "123"
},{
  "name": "saar",
  "password": "saar",
  "api_token": "456"
},{
    "name": "kuuseok",
    "password": "kuuseok",
    "api_token": "789"
}]
```

Just after installation you can use `api_token` listed above. But after each login token will be regenerated.
You can get new token with Swagger by request:
````
POST /api/login
````
In the response you can find `api_token` that you should submit to Swagger:
```json
{
  "data": {
    "id": 1,
    "name": "aleksander",
    "first_name": "Aleksander",
    "last_name": "Skafander",
    "position": "programmijuhi abi",
    "department": "Infotehnoloogia teaduskonna dekanaat",
    "user_id": "d543193bdf256363bf956f83ebd288e53000fe48",
    "api_token": "G57k37hfUZX6kHIEFesuqhUF3LWhPYiGGMMZk2qw2ETOldVvkMVOQ5nCTFcx",
    "created_at": "2018-03-22 16:36:23",
    "updated_at": "2018-03-22 17:41:36"
  }
}
```
Take `api_token` and put it into Swagger authorization form prefixing with `Bearer`:
````
Bearer G57k37hfUZX6kHIEFesuqhUF3LWhPYiGGMMZk2qw2ETOldVvkMVOQ5nCTFcx
````

After Bearer authorization you can make `GET /api/actors` request:
```json
[
  {
    "id": 1,
    "created_at": "2018-03-22 16:36:23",
    "updated_at": "2018-03-22 16:36:23",
    "first_name": "Aleksander",
    "last_name": "Skafander",
    "employee_id": "d543193bdf256363bf956f83ebd288e53000fe48",
    "position": "Programmijuhi abi",
    "department": "Infotehnoloogia teaduskonna dekanaat",
    "role": "employee"
  },
  {
    "id": 2,
    "created_at": "2018-03-22 16:36:23",
    "updated_at": "2018-03-22 16:36:23",
    "first_name": "Aleksandra",
    "last_name": "Saar",
    "employee_id": "2eddfdbbbbcc974ff8c6edc3b7e877396fd86788",
    "position": "Infotehnoloogia teaduskonna dekanaati professor",
    "department": "Infotehnoloogia teaduskonna dekanaat",
    "role": "manager"
  },
  {
    "id": 3,
    "created_at": "2018-03-22 16:36:23",
    "updated_at": "2018-03-22 16:36:23",
    "first_name": "Aleksandra",
    "last_name": "Kuuseok",
    "employee_id": "91adb61eced7eec14a3936da41ad8e0c0cdd76e3",
    "position": "Raamatupidaja",
    "department": "Rahandusosakond",
    "role": "accountant"
  }
]
```

You have to provide `X-Actor-Id` HTTP header to be able to make any other requests.

Just take one of actor's `id` above and put it into Swagger authorization form.

### Finish ###

That's all! Now you can make any other requests. Some of them are restricted by actor's role. 
If so you will get `403 Forbidden` response. Just provide another X-Actor-Id and try again.
